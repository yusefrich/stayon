import { createApp } from 'vue'
import App from './App.vue'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import './assets/css/font/stylesheet.css';
import './assets/css/styles.css';
import 'aos/dist/aos.css'
import { VueMaskDirective } from 'v-mask'
import { ObserveVisibility } from 'vue-observe-visibility';
import router from '../router/index' // <---
import VueFinalModal from 'vue-final-modal'
/* import VueSmoothScroll from 'vue3-smooth-scroll' */
import VueRellax from 'vue-rellax'


const app = createApp(App)

app.directive('observe-visibility', {
  beforeMount: (el, binding, vnode) => {
    vnode.context = binding.instance;
    ObserveVisibility.bind(el, binding, vnode);
  },
  update: ObserveVisibility.update,
  unmounted: ObserveVisibility.unbind,
});
app.directive("mask", VueMaskDirective);
app.use(VueFinalModal())
app.use(VueRellax);

/* app.use(VueSmoothScroll, {
  duration: 400,
  updateHistory: false
}) */


app.use(router)
app.mount('#app')

/* Scrollbar.init(document.querySelector('#app')); */
