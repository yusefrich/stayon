import { createWebHistory, createRouter } from "vue-router";
import Home from "../src/Home.vue";
import Terms from "../src/Terms.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/politicas",
    name: "Terms",
    component: Terms,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
